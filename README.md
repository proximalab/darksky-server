# Серверная часть проекта "Темное небо"

## Описание

Предназначен для сбора данных с сайтов, выполняющих мониторинг загрязнения атмосферного воздуха в г. Красноярске на текущий день, и выдаче этих данных по REST API на различные приложения.
На данный момент, найдены следующие источники данных:

* http://www.krasecology.ru/Operative/Air - ресурс, позволяющий (правда, неофициально) забирать данные в формате JSON.
* http://meteo.krasnoyarsk.ru/Мониторингзагрязнения/tabid/227/Default.aspx - данные в виде таблицы и их необходимо парсить

Для сервера планируется использовать Django 1.9.1 + Django REST Framework + кэширование (скорее всего, это будет memcached).

## API

Доступ к API планируется по следующим адресам:

### Получение списка постов наблюдений

GET http://localhost:8000/api/v1/posts/

http://torwald.pythonanywhere.com/api/v1/posts/


### Список параметров доступных для наблюдения

GET http://localhost:8000/api/v1/params/

http://torwald.pythonanywhere.com/api/v1/params/


### Состояние воздуха

GET http://localhost:8000/api/v1/conditions/

http://torwald.pythonanywhere.com/api/v1/conditions/

Основная функция, которая возвращает данные о состоянии атмосферного воздуха.



