#coding=utf8
from air.models import Post, Param, ParamValue
from rest_framework import routers, serializers, viewsets


class PostSerializer(serializers.ModelSerializer):

    class Meta:
        model = Post
        fields = ('code', 'name', 'description', 'longitude', 'latitude',)


class PostViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Post.objects.filter(active=True)
    serializer_class = PostSerializer


class ParamSerializer(serializers.ModelSerializer):
    class Meta:
        model = Param
        fields = ('code', 'name', 'description', 'measure', 'limit')


class ParamViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Param.objects.filter(active=True)
    serializer_class = ParamSerializer


class ParamValueSerializer(serializers.ModelSerializer):
    class Meta:
        model = ParamValue
        fields = ('timestamp', 'post', 'param', 'value',)


class ConditionsViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = ParamValue.get_last_values()
    serializer_class = ParamValueSerializer


router = routers.DefaultRouter()
router.register(r'posts', PostViewSet, "posts")
router.register(r'params', ParamViewSet)
router.register(r'conditions', ConditionsViewSet)
