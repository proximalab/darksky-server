# coding=utf8
from django.contrib import admin
from air.models import DataSource, Post, Param, ParamValue


class BaseModelAdmin(admin.ModelAdmin):
    actions = ['activate', 'deactivate']

    def activate(self, request, queryset):
        queryset.update(active=True)
        self.message_user(request, u'Активированы')

    activate.short_description = u'Активировать выбранное'

    def deactivate(self, request, queryset):
        queryset.update(active=False)
        self.message_user(request, u'Деактивированы')

    deactivate.short_description = u'Деактивировать выбранное'


class DataSourceAdmin(BaseModelAdmin):
    actions = ['load_data'] + BaseModelAdmin.actions
    list_display = ('code', 'name', 'description', 'command', 'active')
    list_filter = ('active',)

    def load_data(self, request, queryset):
        for item in queryset:
            item.load_data()
        self.message_user(request, u'Данные загружены')

    load_data.short_description = u'Загрузить данные для выбранных источников'


class PostAdmin(BaseModelAdmin):
    actions = ['load_data'] + BaseModelAdmin.actions
    list_display = ('code', 'name', 'description', 'data_source', 'longitude', 'latitude', 'active',)
    list_filter = ('active', 'data_source',)
    filter_horizontal = ('params', )

    def load_data(self, request, queryset):
        for item in queryset:
            item.load_data()
        self.message_user(request, u'Данные загружены')

    load_data.short_description = u'Загрузить данные для выбранных источников'


class ParamAdmin(BaseModelAdmin):
    list_display = ('code', 'name', 'limit', 'measure', 'description', 'active',)
    list_filter = ('active',)


class ParamValueAdmin(admin.ModelAdmin):
    date_hierarchy = 'timestamp'
    list_display = ('timestamp', 'post', 'param', 'value',)
    list_filter = ('post', 'param',)


admin.site.register(DataSource, DataSourceAdmin)
admin.site.register(Post, PostAdmin)
admin.site.register(Param, ParamAdmin)
admin.site.register(ParamValue, ParamValueAdmin)
