#coding=utf8
"""
Загрузка данный с сайта http://www.krasecology.ru/

Данные по каждому посту загружаются с этого URL:
http://www.krasecology.ru/Main/GetAirSensorList/{номер поста}

Например,
Список датчиков поста c их ID в базе ресурса
    KRSK-BEREZ: 3
    KRSK-CHEREM: 5
    KRSK-KUBEKOVO: 6
    KRSK-SEVER: 2
    KRSK-SOLN: 4
Посмотреть список датчиков (и ID датчиков) можно здесь:
http://www.krasecology.ru/Main/GetAirSensorList/{ID_поста}

Список значений датчика
http://www.krasecology.ru/Main/GetAirSensorData/361?timelap=day
"""
import requests
from django.core.management.base import BaseCommand
from django.core.exceptions import ObjectDoesNotExist
from air.models import Post, Param, ParamValue


class Command(BaseCommand):
    help = 'Loads info from krasechology.ru posts'

    # карта связей между постом и датчиками на нем
    POST_PARAM_MAP = {
        'KRSK-BEREZ': {
            'CO': 331,
            'DUST': 337,
            'NO': 333,
            'NO2': 334,
            'SO2': 332,
        },
        'KRSK-CHEREM': {
            'CO': 361,
            'DUST': 367,
            'NO': 363,
            'NO2': 364,
            'SO2': 362,
        },
        'KRSK-KUBEKOVO': {
            'CO': 376,
            'DUST': 382,
            'NO': 378,
            'NO2': 379,
            'SO2': 377,
        },
        'KRSK-SEVER': {
            'CO': 316,
            'DUST': None,
            'NO': 318,
            'NO2': 319,
            'SO2': 317,
        },
        'KRSK-SOLN': {
            'CO': 346,
            'DUST': None,
            'NO': 348,
            'NO2': 349,
            'SO2': 347,
        },
    }

    URL = 'http://www.krasecology.ru/Main/GetAirSensorData/{}?timelap=day'

    def add_arguments(self, parser):
        parser.add_argument('post_code', nargs='+', type=str)

    def load_param_data(self, post, param):
        if param.code not in self.POST_PARAM_MAP[post.code]:
            print 'Skip this param {} for this post {}'.format(param.code, post.code)
            return
        print 'Load {} from {}'.format(param.code, post.code)
        url = self.URL.format(self.POST_PARAM_MAP[post.code][param.code])
        result = requests.get(url)
        if result.status_code != 200:
            print 'ERROR', result.status_code
        print result.json()
        data = result.json()
        if not data['Data']:
            print 'No data found: skip'
            return
        last = data['Data'][-1]['y']
        pv = ParamValue(post=post, param=param, value=last)
        pv.save()

    def handle(self, *args, **options):
        for post_code in options['post_code']:
            print 'Process ', post_code
            try:
                post = Post.objects.get(code=post_code)
            except ObjectDoesNotExist:
                print 'Post {} was not found'.format(post_code)
                continue
            if not post.data_source.active or not post.active:
                continue
            if post.code not in self.POST_PARAM_MAP:
                continue
            for param in post.params.filter(active=True):
                self.load_param_data(post, param)
