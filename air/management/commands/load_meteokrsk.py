#coding=utf8
"""
Загружает данные с meteo.krasnoyarsk.ru
"""
__author__ = 'torwald'
import lxml.html as html

from django.core.management.base import BaseCommand
from django.core.exceptions import ObjectDoesNotExist
from air.models import Post, Param, ParamValue


class Command(BaseCommand):
    help = 'Loads info from krasechology.ru posts'

    URL = 'http://meteo.krasnoyarsk.ru/%D0%9C%D0%BE%D0%BD%D0%B8%D1%82%D0%BE%D1%80%D0%B8%D0%BD%D0%B3%D0%B7%D0%B0%D0%B3%D1%80%D1%8F%D0%B7%D0%BD%D0%B5%D0%BD%D0%B8%D1%8F/tabid/227/Default.aspx'

    POST_MAP = {
        'METEO-1': u'\xe2\x84\x961',
        'METEO-3': u'\xe2\x84\x963',
        'METEO-5': u'\xe2\x84\x965',
        'METEO-7': u'\xe2\x84\x967',
        'METEO-8': u'\xe2\x84\x968',
        'METEO-9': u'\xe2\x84\x969',
        'METEO-20': u'\xe2\x84\x9620',
        'METEO-21': u'\xe2\x84\x9621',
    }

    PARAM_MAP = {
        'CO': u'3',
        'DUST': u'1',
        'NO': u'5',
        'NO2': u'4',
        'SO2': u'2',
    }

    def add_arguments(self, parser):
        parser.add_argument('post_code', nargs='+', type=str)

    def handle(self, *args, **options):
        page = html.parse(self.URL)
        table = page.getroot().cssselect('#dnn_ctr636_HtmlModule_lblContent table tbody')[0]

        table_header = table.getchildren()[2]
        table_data = table.getchildren()[4:]
        # заполняем связь между колонками таблицы и постами наблюдений
        posts = Post.objects.filter(active=True, data_source__active=True, code__in=options['post_code'])
        code2post = {p.code: p for p in posts}

        col2post = {}

        for col_num, th in enumerate(table_header.getchildren()[2:]):
            th_text = th.text_content()
            for post_code, pattern in self.POST_MAP.items():
                if post_code not in code2post:
                    continue
                if th_text.startswith(pattern):
                    col2post[col_num] = code2post[post_code]
                    continue

        for row in table_data:
            row_header = row.getchildren()[0].text_content()
            cur_param = None

            for param_code, pattern in self.PARAM_MAP.items():
                if pattern == row_header:
                    cur_param = param_code
                    break

            if cur_param is None:
                print 'Param for {} was not found'.format(row_header)
                continue

            try:
                param = Param.objects.get(code=cur_param, active=True)
            except ObjectDoesNotExist:
                print 'Param for row {} not found'.format(cur_param)
                continue

            print col2post

            for col_num, data_cell in enumerate(row.getchildren()[2:]):
                cur_post = col2post.get(col_num, None)
                if cur_post is None:
                    continue

                data_text = data_cell.text_content().replace(',', '.')
                if data_text == '-':
                    continue

                if data_text == u'\xd0\xbd\xd0\xbf\xd0\xbe':
                    data_text = 0.0
                try:
                    value = float(data_text)
                except ValueError:
                    print 'Cannot convert this data {}'.format(repr(data_text))
                    continue

                pv = ParamValue(post=cur_post, param=param, value=value)
                pv.save()
