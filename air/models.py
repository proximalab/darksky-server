# coding=utf8
from __future__ import unicode_literals
from django.core.management import call_command
from django.db import models


class BaseModel(models.Model):
    name = models.CharField(verbose_name=u'название', max_length=255)
    description = models.TextField(verbose_name=u'описание', blank=True, null=True)
    active = models.BooleanField(verbose_name=u'активен', default=True)
    code = models.CharField(verbose_name=u'код', max_length=20, unique=True, primary_key=True)

    class Meta:
        abstract = True

    def __unicode__(self):
        return u'{} <{}>'.format(self.name, self.code)

    def save(self, *args, **kwargs):
        self.code = self.code.upper()
        super(BaseModel, self).save(*args, **kwargs)


class DataSource(BaseModel):
    """
    Источники данных.
    Логическая сущность, которая будет использоваться для сбора данных с источников.
    Способ сбора будет реализовываться при помощи management-команды, имя которой
    будет храниться в поле command.
    В поле code хранится уникальный код источника.
    """
    command = models.CharField(verbose_name=u'команда для сбора данных', max_length=255)

    class Meta:
        verbose_name = u'источник данных'
        verbose_name_plural = u'источники данных'
        ordering = ('code', 'name', 'active',)

    def load_data(self):
        codes = [post.code for post in self.post_set.filter(active=True)]
        call_command(self.command, *codes)


class CoordinateField(models.FloatField):
    pass


class Post(BaseModel):
    """
    Посты наблюдения.
    Географические объекты, к которым привязаны
    """
    longitude = CoordinateField(verbose_name=u'долгота')
    latitude = CoordinateField(verbose_name=u'широта')
    data_source = models.ForeignKey('DataSource', verbose_name=u'источник данных')
    params = models.ManyToManyField('Param', related_name='posts', blank=True)

    class Meta:
        verbose_name = u'пост наблюдения'
        verbose_name_plural = u'посты наблюдения'
        ordering = ('code', 'name', 'active',)

    def load_data(self, command=None):
        """
        Выполняет загрузку данных для поста. Для этого вызывает менеджмент-команду, ассоциированную с источником данных.
        В качестве аргумента для этой команды отдает свой код.
        """
        command = command if command else self.data_source.command
        call_command(command, self.code)


class Param(BaseModel):
    """
    Параметры наблюдений.
    """
    measure = models.CharField(verbose_name=u'единица измерения', max_length=255)
    limit = models.FloatField(verbose_name=u'порог допустимого значения')

    class Meta:
        verbose_name = u'параметр'
        verbose_name_plural = u'параметры'
        ordering = ('code', 'name', 'active',)


class ParamValue(models.Model):
    """
    Собственно данные с моментами времени.
    """
    timestamp = models.DateTimeField(verbose_name=u'метка времени', auto_now_add=True)
    post = models.ForeignKey('Post', verbose_name=u'пост наблюдения')
    param = models.ForeignKey('Param', verbose_name=u'параметр')
    value = models.FloatField(verbose_name=u'значение')

    class Meta:
        get_latest_by = 'timestamp'
        verbose_name = u'значение показателя'
        verbose_name_plural = u'значения показателей'
        ordering = ('-timestamp', 'param', 'post',)

    def __unicode__(self):
        return u'{}@{}: {} = {}'.format(self.timestamp, self.post.code, self.param.code, self.value)

    @classmethod
    def get_last_values(self):
        """
        select post_id, param_id, value, max(timestamp) as timestamp from air_paramvalue GROUP BY param_id, post_id
        :return:
        """
        query = """select id, post_id, param_id, value, max(timestamp) as timestamp
        from air_paramvalue GROUP BY param_id, post_id
        """
        return self.objects.raw(query)
