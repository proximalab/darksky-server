#coding=utf8
from django.conf.urls import url, include
import air.API.v1 as APIv1

urlpatterns = [
    url('^v1/', include(APIv1.router.urls, namespace='v1')),
]
